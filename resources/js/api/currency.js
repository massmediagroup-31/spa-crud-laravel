import axios from "axios/index";

export default {
  all() {
    return axios.get(`/api/currency`);
  },
  store(data) {
    return axios.post(`/api/currency/store`, data);
  }
}