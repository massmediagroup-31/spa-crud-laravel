import Vue from 'vue'
import VueRouter from 'vue-router'
import VeeValidate from 'vee-validate';
import PortalVue from 'portal-vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueAxios from 'vue-axios';
import VueSweetalert2 from 'vue-sweetalert2';


Vue.use(VueRouter);
Vue.use(VueSweetalert2);
Vue.use(VeeValidate, {
  errorBagName: 'vErrors'
});
Vue.use(PortalVue);
Vue.use(BootstrapVue);
Vue.use(VueAxios, axios);

axios.defaults.baseURL = 'http://localhost:8090/api';

import App from './views/App'
import About from './views/About'
import Home from './views/Home'
import UsersIndex from './views/UsersIndex'
import CurrencyDesc from './views/CurrencyDesc'
import UsersEdit from './views/UsersEdit'
import NotFound from './views/NotFound'
import UsersCreate from './views/UsersCreate'
import RandomChart from './views/RandomChart'
import Login from './views/Login'
import Register from './views/Register'
import Dashboard from './views/Dashboard'

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
    {
      path: '/users',
      name: 'users.index',
      component: UsersIndex,
    },
    {
      path: '/users/create',
      name: 'users.create',
      component: UsersCreate,
    },
    {
      path: '/currency',
      name: 'currency.desc',
      component: CurrencyDesc,
    },
    {
      path: '/users/:id/edit',
      name: 'users.edit',
      component: UsersEdit,
    },
    {
      path: '/404',
      name: '404',
      component: NotFound,
    },
    {
      path: '*',
      redirect: '/404'
    },
    {
      path: '/chart',
      name: 'random.chart',
      component: RandomChart,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        auth: false
      }
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: {
        auth: false
      }
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        auth: true
      }
    },

  ],
});

Vue.router = router

Vue.use(require('@websanova/vue-auth'), {
  auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
  http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
});

App.router = Vue.router

const app = new Vue({
  el: '#app',
  components: {App},
  router,
});