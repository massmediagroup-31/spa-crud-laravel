<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;

class UsersController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return UserResource::collection(User::paginate(10));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(UserStoreRequest $request)
    {
        $data = $request->all();

        return User::create($data);

    }

    /**
     * @param User $user
     * @return UserResource
     */
    public function edit(User $user)
    {
        return new UserResource($user);
    }

    /**
     * @param User $user
     * @param UserUpdateRequest $request
     * @return UserResource
     */
    public function update(User $user, UserUpdateRequest $request)
    {
        $data = $request->all();
        $user->update($data);

        return new UserResource($user);
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response(null, 204);
    }
}
