<?php

namespace App\Http\Controllers\Api;

use App\Currency;
use App\Http\Resources\CurrencyResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CurrencyController extends Controller
{
    /**
     * @return array
     */
    public function index()
    {
        $pricesUsd = Currency::getDataFromCurrency('usd');
        $pricesGbp = Currency::getDataFromCurrency('gbp');
        $pricesEur = Currency::getDataFromCurrency('eur');
        $dates = Currency::getDataFromCurrency('created_at');
        return ['usd' => $pricesUsd, 'gbp' => $pricesGbp, 'eur' => $pricesEur, 'dates' => $dates];
    }

    public function store(Request $request)
    {
        return Currency::create($request->all());
    }
}
