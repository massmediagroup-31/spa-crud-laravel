<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'usd',
        'gbp',
        'eur',
    ];

    /**
     * @param $field
     * @return array
     */
    public static function getDataFromCurrency($field){
        $arr = Currency::all($field)->toArray();

        // get only current field
        foreach ($arr as $value) {
            $newArr[] = $value[$field];
        }
        return $newArr;
    }

}
