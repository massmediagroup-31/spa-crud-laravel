<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Http\Resources\UserResource;

class UserTest extends TestCase
{
    /** @test */
    public function can_get_all_the_users()
    {
        $this->get('/api/users')
            ->assertStatus(200)
            ->original->toJson();
    }

    /** @test */
    public function can_create_a_user()
    {
        $user = factory(User::class)->make()->toArray();
        $user['password'] ='pass';
        $this->post('/api/users/create', $user)
            ->assertStatus(201);

        $this->assertDatabaseHas('users', [
            'email' => $user['email']
        ]);
    }
}
